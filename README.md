E2E tests boilerplate using webdriver io, node, mocha and chai. 

### Requirements:
https://nodejs.org/en/download/

### Setup:
```
npm install
```
### Config:
```
cp .env.dist .env 
```
### Run:
```
npm test
```
### Creating Allure report:
Java 8+ jre
```
npm install -g allure-commandline
```
After executing `npm test` (at least once).
```
npm run report
```