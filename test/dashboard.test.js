const { expect } = require('chai')

const DashboardPage = require('../page/dashboard.page');
const CreateProgramPage = require('../page/createProgram.page');

describe('Dashboard', () => {
    it('should be on dashboard page', () => {
        DashboardPage.open();

        browser.waitUntil(() => browser.getTitle() === DashboardPage.title, 5000);
        expect(browser.getTitle()).to.equal(DashboardPage.title);
    });

    it('should redirect to new program creating after click new program button', () => {
        DashboardPage.createNewProgram.click();

        browser.waitUntil(() => browser.getTitle() === CreateProgramPage.title, 5000);
        expect(browser.getTitle()).to.equal(CreateProgramPage.title);
    });

    // it('example failed test', () => {
    //     browser.reloadSession();
    //     DashboardPage.open();
    //
    //     DashboardPage.createNewProgram.click();
    //
    //     browser.waitUntil(() => browser.getTitle() === CreateProgramPage.title, 5000);
    //     expect(browser.getTitle()).to.equal('Nonexisting title');
    // });
});