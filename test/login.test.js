const { expect } = require('chai')

const LoginPage = require('../page/login.page');
const DashboardPage = require('../page/dashboard.page');

describe('Login', () => {
    it('should be on login page', () => {
        LoginPage.open();

        browser.waitUntil(() => browser.getTitle() === LoginPage.title, 5000);
        expect(browser.getTitle()).to.equal(LoginPage.title);
    });

    it('should login and redirect to dashboard', () => {
        LoginPage.open();
        LoginPage.login();

        browser.waitUntil(() => browser.getTitle() === DashboardPage.title, 5000);
        expect(browser.getTitle()).to.equal(DashboardPage.title);
    });

    it('should show success message after reset password', () => {
        browser.reloadSession();

        LoginPage.open();
        LoginPage.username.setValue('admin@inis.pl');
        LoginPage.resetPassword();

        expect(LoginPage.successMessage.getText()).to.contain('Wysłaliśmy do Ciebie wiadomość e-mail z linkiem resetującym hasło.');
    });
});