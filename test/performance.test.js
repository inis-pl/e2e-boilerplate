const { expect } = require('chai')

const LoginPage = require('../page/login.page');

describe('Dashboard', () => {
    before(() => {
        browser.enablePerformanceAudits();
    });

    it('should load within performance budget', () => {
        LoginPage.open();
        
        let metrics = browser.getMetrics();
        console.log(metrics);
        expect(metrics.speedIndex).to.lte(10000); 

        let score = browser.getPerformanceScore(); // get Lighthouse Performance score
        expect(score).to.gte(.30);

    });

    after(() => {
        browser.disablePerformanceAudits()
    });
});