class Page {
    get title() {
        return 'INIS | ';
    }

    open(url) {
        browser.url(url);
    }
}

module.exports = Page;