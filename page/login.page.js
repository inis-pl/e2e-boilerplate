const { parsed: env } = require('dotenv').config();

const Page = require('../core/page');

class LoginPage extends Page {
    get title() {
        return super.title + 'Witaj!';
    }

    get username() {
        return $('input[name=username]');
    }

    get password() {
        return $('input[name=password]');
    }

    get successMessage() {
        return $('.success-content');
    }

    submit() {
        $('.button-round').click();
    }

    resetPassword() {
        $('.remind').click();
    }

    login() {
        this.username.setValue(env.USER);
        this.password.setValue(env.PASSWORD);
        this.submit();
    }

    open() {
        super.open(env.BASE_URL);
    }
}

module.exports = new LoginPage();