const Page = require('../core/page');

const LoginPage = require('../page/login.page');

class DashboardPage extends Page {
    get title() {
        return super.title + 'Dashboard';
    }

    get createNewProgram() {
        return $('div.new-program-button');
    }

    open() {
        LoginPage.open();
        LoginPage.login();
    }
}

module.exports = new DashboardPage();