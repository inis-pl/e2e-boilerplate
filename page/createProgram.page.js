const Page = require('../core/page');

class CreateProgramPage extends Page {
    get title() {
        return super.title + 'Stwórz Program';
    }
}

module.exports = new CreateProgramPage();